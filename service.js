
const express = require('express');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();
const jwtMiddleware = require('./middleware/jwtMiddleware')

const app = express();
app.use(express.json());

const port = process.env.SERVICE_PORT
const secret_phrase = process.env.SECRET_PHRASE


app.get('/', (req, res) => {

  const token = req.headers.authorization;

  if (token) {
    jwtMiddleware.verifyJWT(token, (err, decoded) => {
      if (err) {
        console.log(new Date().toUTCString() + ' [Invalid token]')
        return res.status(401).json({ message: 'Invalid Token' });
      }

      console.log(new Date().toUTCString() + ' [Valid token usage for ' + decoded.login + ']')
      res.json({ message: "Data fetched", user: decoded.login });
    });
  } else {
    res.status(401).json({ message: 'Token not provided' });
  }
});

app.listen(port, () => {
  console.log(`Service listening on port ${port}`)
})